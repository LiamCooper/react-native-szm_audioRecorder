package com.reactnativerecordsound;

import android.media.MediaRecorder;
import android.util.Log;

import java.io.IOException;
import java.lang.IllegalStateException;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;

import java.util.Map;

public class ReactNativeRecordSound extends ReactContextBaseJavaModule {

  private static final String LOG_TAG = "RecordModule";
  private MediaRecorder mRecorder = null;

  public ReactNativeRecordSound(ReactApplicationContext reactContext) {
    super(reactContext);
  }

  @Override
  public String getName() {
    return "ReactNativeRecordSound";
  }

  @ReactMethod
  public void startRecord(String filename, Callback errorCallBack, Callback successCallBack) {
    if (mRecorder == null) {
      mRecorder = new MediaRecorder();
      mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
      mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
      mRecorder.setOutputFile(filename);
      mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

      try {
        mRecorder.prepare();
      } catch (IOException e) {
        errorCallBack.invoke(LOG_TAG + e.toString());
        return;
      }
      mRecorder.start();
      successCallBack.invoke(LOG_TAG);
    }
  }

  @ReactMethod
  public void stopRecord(Callback errorCallBack, Callback successCallBack) {
    if (mRecorder != null) {
        try {
          mRecorder.stop();
          mRecorder.release();
          mRecorder = null;
        } catch(IllegalStateException e) {
          errorCallBack.invoke(LOG_TAG + "录音无效");
          return;
        }
        successCallBack.invoke(LOG_TAG + "停止成功");
    }
    else{
        errorCallBack.invoke(LOG_TAG + "无效操作");
    }
  }
}
